import React from 'react'
import { useAuth } from 'react-auth'

function AuthenticatedApp() {
  const {
    logout
  } = useAuth();
  return (
    <div>
      Authenticated
      <button type="button" onClick={logout}>Logout</button>
    </div>
  )
}

export default AuthenticatedApp;