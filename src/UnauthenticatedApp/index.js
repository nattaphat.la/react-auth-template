import React, {useState} from 'react'
import { useAuth } from 'react-auth'

function UnauthenticatedApp() {
  let [data, setData] = useState({})
  const {
    login
  } = useAuth()
  const handleChange = (field) => (e) => {
    setData({
      ...data,
      [field]: e.target.value
    })
  }
  const handleLogin = () => {
    login(data);
  }
  return (
    <div>
      <input type="text" onChange={handleChange('username')} />
      <input type="password" onChange={handleChange('password')} />
      <button type="button" onClick={handleLogin}>Login</button>
    </div>
  )
}

export default UnauthenticatedApp;