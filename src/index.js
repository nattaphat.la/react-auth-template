import React from 'react';
import ReactDOM from 'react-dom';
import { AppProviders } from 'react-auth';
import AuthClient from 'react-auth-loopback';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const API_URL = process.env.REACT_APP_API_URL

function SpinnerComponent() {
  return <div>Loading</div>
}
function GetUserFailComponent() {
  return <div>Get user fail try to refresh page</div>
}

ReactDOM.render(
  <AppProviders
    authClient={AuthClient(API_URL)}
    spinnerComponent={SpinnerComponent}
    GetUserFailComponent={GetUserFailComponent}
  >
    <App />
  </AppProviders>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
